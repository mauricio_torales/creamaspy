import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Inicio from './pages/Inicio';
import Programa from './pages/Programa';
import PlanoBajo from './pages/Plano-bajo';
import PlanoAlto from './pages/Plano-alto';
import Llegar from './pages/Llegar';
import Aliados from './pages/Aliados';
import Detalles from './pages/Detalles';
const FirstActivity_StackNavigator = createStackNavigator({
  //All the screen from the Screen1 will be indexed here
  Inicio: {
   screen: Inicio,
   //screen: Programa,
  },
  Programa: {
    screen: Programa,
  },
  PlanoBajo: {
    screen: PlanoBajo,
  },
  PlanoAlto: {
    screen: PlanoAlto,
  },
  Llegar: {
    screen: Llegar,
  },
  Aliados: {
    screen: Aliados,
  },
  Detalles: {
    screen: Detalles,
  },

});

export default createAppContainer(FirstActivity_StackNavigator);