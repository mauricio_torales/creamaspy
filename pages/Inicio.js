//This is an example code for NavigationDrawer//
import React, { Component } from 'react';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';

//import react in our code.
import {AsyncStorage, Platform, Linking,SafeAreaView,Dimensions,Button,TouchableWithoutFeedback,Image, StyleSheet, View, Text } from 'react-native';
const ancho = Math.round(Dimensions.get('window').width);
// import all basic components
 

async function getToken() {
  // Remote notifications do not work in simulators, only on device
  
  if (!Expo.Constants.isDevice) {
    return;
  }
  const { status: existingStatus } = await Permissions.getAsync(
    Permissions.NOTIFICATIONS
  );
  let finalStatus = existingStatus;

  // only ask if permissions have not already been determined, because
  // iOS won't necessarily prompt the user a second time.
  if (existingStatus !== 'granted') {
    // Android remote notification permissions are granted during the app
    // install, so this will only ask on iOS
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    finalStatus = status;
  }

  // Stop here if the user did not grant permissions
  if (finalStatus !== 'granted') {
    return;
  }

  let token = await Notifications.getExpoPushTokenAsync();
  saveItem('token_device', token);
  console.log('Our token', token);
  /// Send this to a server
  // POST the token to your backend server from where you can retrieve it to send push notifications.
  /*return fetch(PUSH_ENDPOINT, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      token: {
        value: token,
      },
      user: {
        username: 'general',
      },
    })
  }).then((response) => {console.log('response:',response.status)});*/
}

async function saveItem(item, selectedValue) {
  try {
    await AsyncStorage.setItem(item, selectedValue);
  } catch (error) {
    console.error(error);
  }
}



export default class Inicio extends Component {


  async componentDidMount(){
    // channel for android
    if (Platform.OS === 'android') {
       Notifications.createChannelAndroidAsync('general', {
        name: 'General',
        sound: true,
        priority: 'max',
        vibrate: [0, 250, 250, 250],
      });
    }

    setTimeout(async()=> {
     await  getToken();
     }, 1500);
     
     this._notificationSubscription = Notifications.addListener(this._handleNotification);


  }
    static navigationOptions = ({ navigation }) => {
        return {
          header: null,
        };
      };
  render() {
    return (
      <View style={styles.MainContainer}>
          <Image source={require("../assets/img/inicio.png")} style={styles.Cabecera}/>
          <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
            <View style={styles.Menu} >
                <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Programa')}>
                  <Image source={require("../assets/img/menu-programa.png")} style={styles.menuItem1}/>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('PlanoBajo')}>
                  <Image source={require("../assets/img/menu-plano.png")} style={styles.menuItem}/>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Llegar')}>
                  <Image source={require("../assets/img/menu-llegar.png")} style={styles.menuItem}/>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => {Linking.openURL('http://www.creamaspy.com');}}>
                  <Image source={require("../assets/img/menu-web.png")} style={styles.menuItem}/>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Aliados')}>
                  <Image source={require("../assets/img/menu-organizacion.png")} style={styles.menuItem2}/>
                </TouchableWithoutFeedback>
            </View>
            <View style={styles.Redes,{flex:0.2, flexDirection: 'row', marginTop:150, justifyContent:"center"}}>
            <TouchableWithoutFeedback onPress={() => {
            Linking.openURL('https://www.facebook.com/creamaspy/');}}>
              <View style={{width: 50, height: 50,marginRight:30 }}>
                  <Image source={require("../assets/img/fb.png")} style={styles.iconoRedes}/>
              </View>
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback onPress={() => {
            Linking.openURL('https://www.instagram.com/creamaspy/');}}>
              <View style={{width: 50, height: 50,marginLeft:50,marginRight:50 }} >
                  <Image source={require("../assets/img/insta.png")} style={styles.iconoRedes}/>
              </View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={() => {
            Linking.openURL('https://www.twitter.com/creamaspy/');}}>
              <View style={{width: 50, height: 50,marginLeft:30 }} >
                  <Image source={require("../assets/img/tw.png")} style={styles.iconoRedes}/>
              </View>
            </TouchableWithoutFeedback>
          </View>
          </SafeAreaView>
         
      </View>
    );
  }
}
 
const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    alignItems: 'center',
  },
  Cabecera:{
      marginTop:25,
      width:ancho,
      height:150, 
  },
  Menu:{
    flex: 1,
    marginTop:20,
    alignItems: 'center',
    width:ancho-40,
  },
  menuItem1:{
    width:'100%',
    height:90,
},
  menuItem:{
      width:'100%',
      height:90,
  },
  menuItem2:{
    width:'100%',
    height:40,
  },
  Redes:{
    marginBottom:20
  },
  iconoRedes:{
    width:'100%',
    height:50,
  },

});