//This is an example code for NavigationDrawer//
import React, { Component } from 'react';
//import react in our code.
import {TouchableWithoutFeedback,Dimensions,Image, StyleSheet, View, Text } from 'react-native';
const ancho = Math.round(Dimensions.get('window').width);
// import all basic components
 
export default class Plano extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
          header: null,
        };
      };
  render() {
    return (
      <View style={styles.MainContainer}>
          <Image source={require("../assets/img/aliados.png")} style={styles.Cabecera}/>
          <Image source={require("../assets/img/organiza.png")} style={styles.Organiza}/>
          <Image source={require("../assets/img/presenta.png")} style={styles.Presenta}/>
          <Image source={require("../assets/img/auspicia.png")} style={styles.Auspicia}/>
         
          <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Inicio')}>
            <Image source={require("../assets/img/atras.png")} style={styles.Atras}/>
          </TouchableWithoutFeedback>
          <Image />
      </View>
    );
  }
}
 
const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    alignItems: 'center',
  },
  Cabecera:{
      marginTop:0,
      width:ancho+2,
      height:320, 
  },
  Organiza:{
    width:ancho+2,
    height:100,
    marginTop:-60,
  },
  Presenta:{
    width:ancho+2,
    height:80
  },
  Auspicia:{
    width:ancho+2,
    height:250
  },
  Atras:{
    width:150,
    height:50,
    position:"absolute",
    bottom:10
  }
});