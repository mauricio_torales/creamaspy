//This is an example code for NavigationDrawer//
import React, { Component } from 'react';
//import react in our code.
import {TouchableWithoutFeedback,Dimensions,Image, StyleSheet, View, Text } from 'react-native';
import { CheckBox } from 'react-native-elements';
const ancho = Math.round(Dimensions.get('window').width);
// import all basic components
 
export default class Plano extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
          header: null,
        };
      };
  render() {
    return (
      <View style={styles.MainContainer}>
          <Image source={require("../assets/img/plano.png")} style={styles.Cabecera}/>
          <Image source={require("../assets/img/plano.jpeg")} style={styles.Plano}/>
          <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Inicio')}>
            <Image source={require("../assets/img/atras.png")} style={styles.Atras}/>
          </TouchableWithoutFeedback>
          <Image />
      </View>
    );
  }
}
 
const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    alignItems: 'center',
  },
  Cabecera:{
      marginTop:39,
      width:ancho+2,
      height:320, 
  },
  Plano:{
    width:ancho,
    height:300, 
    marginTop:-35
  },
  Btn:{
    width:150,
    height:50,
    position:"absolute",
    top:330
  },
  Atras:{
    width:150,
    height:50,
    position:"absolute",
    bottom:10
  }
});