//This is an example code for NavigationDrawer//
import React, { Component } from 'react';
//import react in our code.
import {Dimensions,Image, StyleSheet, View, Text, ScrollView } from 'react-native';
import Stars from 'react-native-stars';
const ancho = Math.round(Dimensions.get('window').width);
// import all basic components
 
export default class Plano extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
          header: null,
        };
      };
  render() {
    return (
      <ScrollView style={{width:ancho,height:1000}}>
        <View style={styles.MainContainer}>
            <Image source={require("../assets/img/programa.png")} style={styles.Cabecera}/>
            
            <View style={styles.infoCabecera,{flex: 1,flexDirection: 'row'}}>
                <View style={{width: '40%', height:135 }}>
                    <Image source={require("../assets/img/personaje.png")} style={styles.Perfil}/>
                </View>
                <View style={{width: '60%',flex: 1, flexDirection: 'column' }}>
                    <View style={styles.infoCabecera,{ flexDirection: 'row',height:29}}>
                        <View style={{width: '85%' }}>
                            <Image source={require("../assets/img/jueves-2.png")} style={styles.Separadorsito}/>
                        </View>
                        <View style={{alignItems:'center',justifyContent:'center' ,width: '15%',backgroundColor:'#0C419A',}}>
                          <Stars
                            half={false}
                            default={0}
                            update={(val)=>{this.setState({stars: val})}}
                            spacing={4}
                            starSize={20}
                            count={1}
                            fullStar= {require('../assets/img/estrella-full.png')}
                            emptyStar= {require('../assets/img/estrella-vacia.png')}/>
                        </View>
                    </View>

                    
                    <View style={{ width: '100%', height:30,backgroundColor:'white',alignItems: 'center',flexDirection: 'row',justifyContent:'center',alignContent:'center' }}>
                      <Text style={{ width: '20%',justifyContent:'center',alignContent:'center'}} >14:00</Text>
                      <Text style={{ width: '50%',justifyContent:'center',alignContent:'center'}} >ARTES VISUALES</Text>
                      <Text style={{ width: '30%',justifyContent:'center',alignContent:'center'}} >SALA B</Text>
                    </View>  
                    <View style={{ width: '100%', height:35,backgroundColor:'white',alignContent: 'stretch', }}>
                      <Image source={require("../assets/img/separador.png")} style={styles.Separador}/>
                    </View>  
                    <View style={{ width: '100%',marginTop:-3.5, height:44,alignItems: 'center',alignContent: 'stretch',flexDirection: 'row',justifyContent:'center',alignContent:'center' }}>
                      <Text style={{ width: '70%',justifyContent:'center',alignContent:'center'}} >LUCAS TORÑO</Text>
                      <Text style={{ width: '30%',justifyContent:'center',alignContent:'center'}} >Paraguay</Text>
                    </View>  
                </View>
            </View>
            <View style={styles.infoCabecera,{flex: 1,flexDirection: 'row' }}>
                  <View style={{backgroundColor:'#F9DA4A' ,width: '100%',marginBottom:20,marginTop:-2, height:44,alignItems: 'center',alignContent: 'stretch',flexDirection: 'row',justifyContent:'center',alignContent:'center' }}>
                      <Text style={{justifyContent:'center',alignContent:'center'}} >MANAGEMENT, REDES E INTERNACIONALIZACIÓN DE ARTISTAS</Text>
                  </View>
            </View>
            <Image source={require("../assets/img/curri.png")} style={styles.Curri}/>
            
        </View>
      </ScrollView>
    );
  }
}
 
const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent:'center'
  },
  Cabecera:{
      marginTop:39,
      width:ancho,
      height:230, 
  },
  Curri:{
    width:ancho,
    height:300,
  },
  Perfil:{
    resizeMode:'contain',
    flex:1,
    width:'100%',
  },
  Separadorsito:{
    resizeMode:'contain',
    flex:1,
    width:'100%',
  },
  Separador:{
    resizeMode:'contain',
    flex:1,
    width:'100%',
  },
  Titulo:{
    width:ancho,
    height:0,
    marginTop:-250,
    backgroundColor:'#F9DA4A',
  },
  Atras:{
    width:150,
    height:50,
    position:"absolute",
    bottom:10
  }
});