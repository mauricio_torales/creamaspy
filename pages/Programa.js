//This is an example code for NavigationDrawer//
import React, { Component } from 'react';
//import react in our code.
import {TouchableHighlight,Dimensions,Image, StyleSheet, View, Text, AsyncStorage } from 'react-native';
import { CheckBox } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
const ancho = Math.round(Dimensions.get('window').width);
// import all basic components
var tempCheckValues = [];

export default class Programa extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
          header: null,
        };
      };
      constructor(props) {
        super(props);
    
        this.state = {
          checkBoxChecked: [],
          dataChech : [],
          marcados : [],
          programa : [],
          programaOriginal: [],
          filtro : 'todos'
        }
      }

      handleChangeCheck= (index, id) => {
        let checkBoxChecked = [...this.state.checkBoxChecked];
        let dataChech = [...this.state.dataChech];
        checkBoxChecked[index] = !checkBoxChecked[index];
        dataChech[index] = !dataChech[index];
        this.setState({ 
          checkBoxChecked,
          dataChech
        }, async ()=>{
            const token = await AsyncStorage.getItem('token_device');
            if(dataChech[index]){
                try {
                    const response = await fetch('http://eventos.intraparaguay.com/appFavoritos/addAjaxApp', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                            AppFavorito : {
                                app_programa_evento_id:id,
                                programa_evento_id:id,
                                token_device : token
                            }
                        }),
                    });
                    
                
                    const responseJson = await response.json();
                    if(responseJson != null){
                        console.log(responseJson)
                    }
                }
                catch (error) {
                    console.error(error);
                }
            }else{
                try {
                    const response = await fetch('http://eventos.intraparaguay.com/appFavoritos/deleteAjaxApp', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                            AppFavorito : {
                                app_programa_evento_id:id,
                                programa_evento_id:id,
                                token_device : token
                            }
                        }),
                    });
                    
                
                    const responseJson = await response.json();
                    if(responseJson != null){
                        console.log(responseJson)
                    }
                }
                catch (error) {
                    console.error(error);
                }
            }
        });
      }

    async componentDidMount(){ 

        const response = await fetch('http://eventos.intraparaguay.com/appProgramaEventos/programas/');
        const responseJson = await response.json();
        await this.setState({
            programa : responseJson,
            programaOriginal : responseJson
        }, async ()=>{
            var ch = [];
            
            this.state.programa.map(function(item){
                //ch.push({checked:false})
                ch[item.AppProgramaEvento.id] = false; 

            });

            this.setState({
                dataChech:ch
            }, async ()=>{
                await this.getMarcados();  
            });
        })
        
    }
    
      async getMarcados(){
        try {
            const token = await AsyncStorage.getItem('token_device');
            const response = await fetch('http://eventos.intraparaguay.com/appFavoritos/getAjaxApp', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                    AppFavorito : {
                        token_device : token
                    }
                }),
            });
        
           const responseJson = await response.json();
            if(responseJson.marcados != null){
                this.setState({
                    marcados:responseJson.marcados
                }, async () =>{
                    let { dataChech} = this.state;
                    let intialCheck = dataChech.map(x => false);
            
                    await this.state.marcados.map((data) =>{
                        intialCheck[data.AppFavorito.programa_evento_id-1] = true
                        this.state.dataChech[data.AppFavorito.programa_evento_id-1] = true
                    });
                    await this.setState({ 
                        checkBoxChecked: intialCheck, 
                    }, ()=>{
                        console.log(this.state.checkBoxChecked)
                    });  
                });
            }
        }
        catch (error) {
            console.error(error);
        }
      }

    renderCat(filtro){
        console.log(filtro)
        this.setState({

        },()=>{
            let recorrerPrograma = [];
            this.state.programaOriginal.map((i,u) =>  {
                if (i.AppProgramaEvento.categoria==filtro && i.AppProgramaEvento.dia !='2019-10-24') {
                    recorrerPrograma.push(i);
                }

                this.setState({programa:recorrerPrograma});
            })
        });
    }

    renderDia(filtro){
        console.log(filtro)
        this.setState({

        },()=>{
            let recorrerPrograma = [];
            this.state.programaOriginal.map((i,u) =>  {
                if (i.AppProgramaEvento.dia==filtro) {
                    recorrerPrograma.push(i);
                }

                this.setState({programa:recorrerPrograma});
            })
        });
    }

    
  render() {
    return (
        <View style={styles.MainContainer}>
        <ScrollView>
          <Image source={require("../assets/img/programa.png")} style={styles.Cabecera}/>
          <View style={styles.Redes,{ flexDirection: 'row',justifyContent:'center',marginTop:-20}}>
                    <TouchableHighlight onPress={() => this.renderDia('2019-10-24')}>
                         <View style={{width:110,}}>
                             <Image source={require("../assets/img/dia-1.png")} style={styles.iconoDias}/>
                         </View>
                 </TouchableHighlight>
                 <TouchableHighlight onPress={() => this.renderDia('2019-10-25')}>
                         <View style={{width:110,marginLeft:10,marginRight:10}}>
                             <Image source={require("../assets/img/dia-2.png")} style={styles.iconoDias}/>
                         </View>
                 </TouchableHighlight>
                 <TouchableHighlight onPress={() => this.renderDia('2019-10-26')}>
                         <View style={{width:110,}}>
                             <Image source={require("../assets/img/dia-3.png")} style={styles.iconoDias}/>
                         </View>
                 </TouchableHighlight>
             </View>
             <Image source={require("../assets/img/sector.png")} style={styles.tituloSector}/>
             <View style={styles.Redes,{ flexDirection: 'row',justifyContent:'center',marginTop:-50}}>
                
                    <TouchableHighlight onPress={() => this.renderCat('MULTISECTOR')}>
                    <View style={{width:110,}}>
                        <Image source={require("../assets/img/multi.png")} style={styles.iconoDias}/>
                        </View>
                    </TouchableHighlight>
                
                
                    <TouchableHighlight onPress={() => this.renderCat('DISEÑO') }>   
                    <View style={{width:110,marginLeft:10,marginRight:10}}>
                        <Image source={require("../assets/img/design.png")} style={styles.iconoDias}/>
                        </View>
                    </TouchableHighlight>
                
                
                    <TouchableHighlight onPress={() => this.renderCat('AUDIOVISUAL')}>    
                    <View style={{width:110,}}>    
                                <Image source={require("../assets/img/audiovisual.png")} style={styles.iconoDias}/>
                    </View>
                    </TouchableHighlight>
                
             </View>
             <View style={styles.Redes,{ flexDirection: 'row',justifyContent:'center',marginTop:-50}}>
                
                    <TouchableHighlight onPress={() => this.renderCat('PUBLICIDAD') }>  
                    <View style={{width:110,}}>
                             <Image source={require("../assets/img/publicidad.png")} style={styles.iconoDias}/>
                             </View>
                    </TouchableHighlight>
                 
                 <TouchableHighlight onPress={() => this.renderCat('MÚSICA') }>
                         <View style={{width:110,marginLeft:10,marginRight:10}}>
                             <Image source={require("../assets/img/musica.png")} style={styles.iconoDias}/>
                         </View>
                 </TouchableHighlight>
                 <TouchableHighlight onPress={() => this.renderCat('SOFTWARE/APP')  }>
                         <View style={{width:110,}}>
                             <Image source={require("../assets/img/soft.png")} style={styles.iconoDias}/>
                         </View>
                 </TouchableHighlight>
             </View>
             <View style={styles.Redes,{ flexDirection: 'row',justifyContent:'center',marginTop:-50}}>
                 <TouchableHighlight onPress={() => this.renderCat('INVENTOS') }>
                         <View style={{width:110,}}>
                             <Image source={require("../assets/img/inventos.png")} style={styles.iconoDias}/>
                         </View>
                 </TouchableHighlight>
                 <TouchableHighlight onPress={() => this.renderCat('VIDEOJUEGOS') } >
                         <View style={{width:110,marginLeft:10,marginRight:10}}>
                             <Image source={require("../assets/img/juegos.png")} style={styles.iconoDias}/>
                         </View>
                 </TouchableHighlight>
                 <TouchableHighlight onPress={() => this.renderCat('ARTES ESCÉNICAS') }>
                         <View style={{width:110,}}>
                             <Image source={require("../assets/img/artes.png")} style={styles.iconoDias}/>
                         </View>
                 </TouchableHighlight>
             </View>
             <View style={styles.Redes,{ flexDirection: 'row',justifyContent:'center',marginTop:-50}}>
                 <TouchableHighlight onPress={() => this.renderCat('COMIC') }>
                         <View style={{width:110,}}>
                             <Image source={require("../assets/img/comic.png")} style={styles.iconoDias}/>
                         </View>
                 </TouchableHighlight>
                 <TouchableHighlight onPress={() => this.renderCat('GASTRONOMIA') }>
                         <View style={{width:110,marginLeft:10,marginRight:10}}>
                             <Image source={require("../assets/img/gastronomia.png")} style={styles.iconoDias}/>
                         </View>
                 </TouchableHighlight>
                 <TouchableHighlight onPress={() => this.renderCat('ARTES VISUALES') }>
                         <View style={{width:110,}}>
                             <Image source={require("../assets/img/visuales.png")} style={styles.iconoDias}/>
                         </View>
                 </TouchableHighlight>
             </View>
             <Image source={require("../assets/img/marcar.png")} style={styles.tituloSector}/>
                        {this.state.programa.map((i,u) => {
                                let diser = 'DISERTANTES';
                                if (i.AppProgramaEvento.categoria=='MÚSICA') {
                                    diser = '';
                                }
                                return(
                                    <View key={i.AppProgramaEvento.id} style={{marginTop:60}}>
                                    <View style={styles.Redes,{ flexDirection: 'row',justifyContent:'center',marginTop:-50}} >
                                        <View style={{width:50, height: 60, marginTop:-10}}>
                                            <CheckBox
                                                center
                                                checkedIcon='dot-circle-o'
                                                uncheckedIcon='circle-o'
                                                checked={this.state.checkBoxChecked[i.AppProgramaEvento.id-1]}
                                                onPress={() => this.handleChangeCheck(i.AppProgramaEvento.id-1,i.AppProgramaEvento.id)}
                                                checkedColor='red'
                                            />
                                        </View>
                                        <View style={{width:'35%',paddingTop:8,paddingLeft:3,paddingBottom:0,backgroundColor:i.AppProgramaEvento.color,alignContent:'center'}}>
                                            <Text style={{color:'#fff',fontSize:20,fontWeight:'bold'}}>{i.AppProgramaEvento.categoria}</Text>
                                        </View>
                                        <View style={{width:'50%',paddingTop:8,paddingLeft:3,paddingBottom:0,backgroundColor:'#1D4E93'}}>
                                            <Text style={{color:'#fff',fontSize:20,fontWeight:'bold'}} >{i.AppProgramaEvento.tipo}</Text>
                                        </View>
                                    </View>
                                    <View style={styles.Redes,{ flexDirection: 'row',justifyContent:'center',marginTop:5}}>
                                        <View style={{width:'70%',alignContent:'center'}}>
                                            <Text style={{color:'#D21C19',fontSize:20,fontWeight:'bold'}}>{i.AppProgramaEvento.nombre}</Text>
                                        </View>
                                    </View>
                                    <View style={styles.Redes,{ flexDirection: 'row',justifyContent:'center',marginTop:5}}>
                                        <View style={{width:'70%',alignContent:'center'}}>
                                            <Text style={{color:'#1D4E93',fontSize:20}}><Text style={{fontWeight:'bold'}} >{diser}</Text> {i.AppProgramaEvento.disentantes}</Text>
                                        </View>
                                    </View>
                                    <View style={styles.Redes,{ flexDirection: 'row',justifyContent:'center',marginTop:5}}>
                                        <View style={{width:'30%',alignContent:'center'}}>
                                            <Text style={{color:'#D21C19',fontSize:15}}>{i.AppProgramaEvento.hora_inicio_fin}</Text>
                                        </View>
                                        <View style={{width:'40%',alignContent:'center'}}>
                                            <Text style={{color:'#D21C19',fontSize:15}}>{i.AppProgramaEvento.lugar}</Text>
                                        </View>
                                    </View>
                                    </View>
        
                                )                 
                            }
                        )
                    }
          <TouchableHighlight onPress={() => this.props.navigation.navigate('Inicio')}>
            <Image source={require("../assets/img/atras.png")} style={styles.Atras}/>
          </TouchableHighlight>   
          </ScrollView>

          </View>
    );
  }
}
 
const styles = StyleSheet.create({
    MainContainer: {
      flex: 1,
      alignItems: 'center',
    },
    Cabecera:{
        marginTop:39,
        width:ancho,
        height:180
    },
    iconoDias:{
      width:'100%',
      resizeMode:'contain'
    },
    tituloSector:{
      width:ancho, 
      resizeMode:'contain',
      marginTop:-50
    },
    
    Atras:{
      width:150,
      height:50,
      marginLeft:"30%",
      marginTop:20,
      marginBottom:10,
    }
    
  
  });