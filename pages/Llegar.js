//This is an example code for NavigationDrawer//
import React, { Component } from 'react';
//import react in our code.
import {WebView,TouchableWithoutFeedback,Dimensions,Image, StyleSheet, View, Text } from 'react-native';
import openMap from 'react-native-open-maps';
const ancho = Math.round(Dimensions.get('window').width);
// import all basic components
 
export default class LLegar extends Component {
      irMcal() {
        openMap({ latitude: -25.295697, longitude: -57.581380 });
      }
    static navigationOptions = ({ navigation }) => {
        return {
          header: null,
        };
      };
  render() {
    return (
      <View style={styles.MainContainer}>
          <Image source={require("../assets/img/llegar.png")} style={styles.Cabecera}/>
          <View style={styles.ConMapa}>
          <TouchableWithoutFeedback onPress={this.irMcal}>
            <Image source={require("../assets/img/mapa.png")} style={{width:'100%',resizeMode:'contain',height:300}}/>
          </TouchableWithoutFeedback>
          </View> 
          <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Inicio')}>
            <Image source={require("../assets/img/atras.png")} style={styles.Atras}/>
          </TouchableWithoutFeedback>
      </View>
    );
  }
}
 
const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    alignItems: 'center',
  },
  Cabecera:{
      marginTop:39,
      width:ancho+2,
      height:300, 
  },
  ConMapa:{
    width:ancho+2,
    height:450,
    backgroundColor:"red",
    padding:30,
    paddingBottom:100,

  },
  Atras:{
    width:150,
    height:50,
    position:"absolute",
    bottom:10
  }
});